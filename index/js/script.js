let auth = {};
let isAuth = false;

(() => {
    authButtons();
    navButtons();
    openHome();
})();

async function authButtons() {
    let nickname = document.querySelector('#nickname');
    let signin = document.querySelector('#signin');
    let signup = document.querySelector('#signup');
    nickname.onclick = null;
    signin.onclick = null;
    signup.onclick = null;
    nickname.classList.remove('d-none');
    signin.classList.remove('d-none');
    signup.classList.remove('d-none');
    if (await getIsAuth()) {
        nickname.innerText = auth.login;
        nickname.onclick = openNickName;
        signin.classList.add("d-none");
        signup.classList.add("d-none");
    } else {
        nickname.classList.add("d-none");
        signin.onclick = openSignIn;
        signup.onclick = openSignUp;
    }
}

function navButtons() {
    document.querySelector('nav .home').onclick = openHome;
    document.querySelector('nav > .cart').onclick = openCart;
}

async function openHome() {
    let aside = document.querySelector('aside');
    aside.innerHTML = "";
    let products = await getProducts();
    products.forEach(product => addProduct(product));
}

async function getProducts() {
    return await fetch('/product')
        .then(d => d.json())
        .then(data => {
            if (data.meta.status !== 200) {
                return [];
            }
            return data.data;
        });
}

function addProduct(product) {
    let aside = document.querySelector('aside');
    aside.addCustomElements([
        ['div', {
            class: ["product"], children: [
                ['div', {
                    class: ["img"], style: "background-image: url(" + product.img + ");"
                }],
                ['div', {class: ["name"], text: product.name}],
                ['div', {
                    class: ["footer"], children: [
                        ['div', {class: ["price"], text: product.price + ' ' + product.currency}],
                        ['div', {class: ["buy"], text: "BUY", on: {click: () => addOrder(product)}}],
                    ]
                }],
            ]
        }]
    ]);
}

function addOrder(product) {
    let cart = JSON.parse(localStorage.cart || "{}");
    if (cart[product.id] == null) {
        cart[product.id] = {
            id: product.id,
            count: 1,
        };
    }
    localStorage.cart = JSON.stringify(cart);
}

function editCountProduct(productId, field) {
    let cart = JSON.parse(localStorage.cart || "{}");
    let count = +field.value;
    if (isNaN(count)) {
        field.value = cart[productId].count;
        return;
    }
    cart[productId].count = count;
    localStorage.cart = JSON.stringify(cart);
}

function deleteOrder(productId) {
    let cart = JSON.parse(localStorage.cart || "{}");
    delete cart[productId];
    localStorage.cart = JSON.stringify(cart);
    renderOrders();
}

function openCart() {
    let aside = document.querySelector('aside');
    aside.innerHTML = "";
    aside.addCustomElements([
        ['div', {
            class: ['cart'], children: [
                ['div', {class: ['order-list']}],
                ['div', {
                    class: ['info'], children: [
                        ['div', {
                            class: ['info-list'], children: [
                                ['div', {class: ['info-item'], text: auth.lastName}],
                                ['div', {class: ['info-item'], text: auth.firstName}],
                                ['div', {class: ['info-item'], text: auth.middleName}],
                                ['div', {class: ['info-item'], text: auth.address}],
                            ]
                        }],
                        ['div', {
                            class: ['order-btn'], children: [
                                ['input', {type: 'button', class: ['order'], value: "ORDER", on: {click: createOrder}}],
                            ]
                        }],
                    ]
                }],
            ]
        }],
    ]);
    renderOrders();
}

function createOrder() {
    let orders = getOrder();
    fetch('/order', {
        method: 'post',
        body: JSON.stringify(orders)
    })
        .then(d => d.json())
        .then(data => {
            if (data.meta.status !== 200) {
                alert(data.meta.message);
                return;
            }
            delete localStorage.cart;
            renderOrders();
            window.open("/order/print?id=" + data.data.id + "&order=" + encodeURIComponent(data.data.order));
        });
}

async function renderOrders() {
    let orderList = document.querySelector('.order-list');
    orderList.innerHTML = "";
    let orders = [];
    for (const order of getOrder()) {
        orders.push(await renderOrder(order));
    }
    orderList.addCustomElements(orders);
}

function getOrder() {
    let cart = JSON.parse(localStorage.cart || "{}");
    return Object.keys(cart).map(key => cart[key]);
}

async function renderOrder(order) {
    let product = await getProduct(order.id);
    return ['div', {
        class: ['order-item'], children: [
            ['div', {class: ['img'], style: "background-image: url(" + product.img + ");"}],
            ['div', {
                class: ['info'], children: [
                    ['div', {class: ['name'], text: product.name}],
                    ['div', {class: ['price'], text: product.price + ' ' + product.currency}],
                ]
            }],
            ['div', {
                class: ['control'], children: [
                    ['input', {
                        type: 'text',
                        class: ['count'],
                        value: order.count,
                        on: {input: e => editCountProduct(order.id, e.target)}
                    }],
                    ['input', {
                        type: 'button',
                        class: ['remove'],
                        value: 'X',
                        on: {click: () => deleteOrder(order.id)}
                    }],
                ]
            }],
        ]
    }];
}

async function getProduct(id) {
    return await fetch('/product?id=' + id)
        .then(d => d.json())
        .then(data => {
            if (data.meta.status !== 200) {
                return {
                    id: null,
                    name: null,
                    price: null,
                    currency: null,
                    img: null,
                };
            }
            return data.data;
        });
}

function openNickName() {
    //alert("openNickName");
    logout();
}

function openSignIn() {
    let popup = document.querySelector('.popup');
    popup.innerHTML = "";
    popup.addCustomElements([
        ['div', {
            class: ["login-window"], children: [
                ['input', {id: "login-input", type: 'text', placeholder: 'Login'}],
                ['input', {id: "password-input", type: 'text', placeholder: 'Password'}],
                ['input', {class: ["btn-login"], type: 'button', value: 'LOGIN', on: {click: login}}],
                ['input', {class: ["btn-signup"], type: 'button', value: 'SIGNUP', on: {click: openSignUp}}],
            ], on: {click: e => e.stopPropagation()}
        }]
    ]);
    popup.classList.remove("d-none");
    popup.addEventListener('click', closePopup)
}

function openSignUp() {
    let popup = document.querySelector('.popup');
    popup.innerHTML = "";
    popup.addCustomElements([
        ['div', {
            class: ["signup-window"], children: [
                ['input', {id: "login-input", type: 'text', placeholder: 'Login'}],
                ['input', {id: "password-input", type: 'text', placeholder: 'Password'}],
                ['input', {id: "repassword-input", type: 'text', placeholder: 'Password'}],
                ['input', {id: "lastname-input", type: 'text', placeholder: 'Last Name'}],
                ['input', {id: "firstname-input", type: 'text', placeholder: 'First Name'}],
                ['input', {id: "middlename-input", type: 'text', placeholder: 'Middle Name'}],
                ['input', {id: "address-input", type: 'text', placeholder: 'Address'}],
                ['input', {class: ["btn-signup"], type: 'button', value: 'SIGNUP', on: {click: signup}}],
                ['input', {class: ["btn-login"], type: 'button', value: 'LOGIN', on: {click: openSignIn}}],
            ], on: {click: e => e.stopPropagation()}
        }]
    ]);
    popup.classList.remove("d-none");
    popup.addEventListener('click', closePopup)
}

function closePopup() {
    let popup = document.querySelector('.popup');
    popup.innerHTML = "";
    popup.classList.add("d-none");
}

async function getIsAuth() {
    let token = getCookie('token');
    return await fetch('/auth', {
        method: 'post',
        body: JSON.stringify({
            token: token,
        })
    })
        .then(d => d.json())
        .then(data => {
            if (data.meta.status !== 200) {
                return false;
            }
            auth = data.data;
            return true;
        });
}

function login() {
    let loginInput = document.querySelector('#login-input');
    let passwordInput = document.querySelector('#password-input');
    fetch('/auth/login', {
        method: 'post',
        body: JSON.stringify({
            login: loginInput.value,
            password: passwordInput.value,
        })
    })
        .then(d => d.json())
        .then(data => {
            if (data.meta.status !== 200) {
                alert(data.meta.message);
                return;
            }
            setCookie('token', data.data.token);
            authButtons();
            closePopup();
        });
}

function signup() {
    let loginInput = document.querySelector('#login-input');
    let passwordInput = document.querySelector('#password-input');
    let repasswordInput = document.querySelector('#repassword-input');
    let lastnameInput = document.querySelector('#lastname-input');
    let firstnameInput = document.querySelector('#firstname-input');
    let middlenameInput = document.querySelector('#middlename-input');
    let addressInput = document.querySelector('#address-input');
    fetch('/auth/reg', {
        method: 'post',
        body: JSON.stringify({
            login: loginInput.value,
            password: passwordInput.value,
            rePassword: repasswordInput.value,
            lastName: lastnameInput.value,
            firstName: firstnameInput.value,
            middleName: middlenameInput.value,
            address: addressInput.value,
        })
    })
        .then(d => d.json())
        .then(data => {
            if (data.meta.status !== 200) {
                alert(data.meta.message);
                return;
            }
            closePopup();
        });
}

function logout() {
    deleteCookie('token')
    authButtons();
}