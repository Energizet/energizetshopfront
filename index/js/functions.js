Element.prototype.addCustomElement = function (tag, option = null) {
    let child = document.createElement(tag);
    if (option) {
        if (option.id) child.id = '' + option.id;
        if (option.type) child.type = '' + option.type;
        if (option.text) child.innerText = '' + option.text;
        if (option.value) child.value = '' + option.value;
        if (option.placeholder) child.placeholder = '' + option.placeholder;
        if (option.style) child.style = '' + option.style;
        if (option.href) child.href = '' + option.href;
        if (option.src) child.src = '' + option.src;
        if (option.class) {
            option.class.forEach((item) => {
                if (item !== '') {
                    child.classList.add(item);
                }
            });
        }
        if (option.selected) child.selected = option.selected;
        if (option.title) child.title = option.title;
        if (option.on) {
            Object.keys(option.on).forEach((eventName) => {
                child['on' + eventName] = option.on[eventName];
            });
        }
        if (option.children) child.addCustomElements(option.children)
    }
    this.appendChild(child);
    return child;
};
Element.prototype.addCustomElements = function (elements) {
    elements.forEach(element => {
        let view = this.addCustomElement(element[0], element[1]);
        this.appendChild(view);
    });
};

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {

    options = {
        path: '/',
        // при необходимости добавьте другие значения по умолчанию
        ...options
    };

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, "", {
        'max-age': -1
    })
}