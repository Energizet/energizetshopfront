(() => {
    fetch(location.pathname + location.search, {
        method: 'post',
    })
        .then(d => d.json())
        .then(data => {
            renderOrder(data.data);
        });
})();

function renderOrder(data) {
    let orderId = document.querySelector('.order-id');
    let fio = document.querySelector('.fio');
    let address = document.querySelector('.address');
    orderId.innerHTML = "Order №" + data.id;
    fio.innerHTML = data.user.lastName + " " + data.user.firstName + " " + data.user.middleName;
    address.innerHTML = data.user.address;

    let orderList = document.querySelector('.order-list');
    orderList.addCustomElements(data.products.map(item =>
        ['div', {
            class: ['order-item'], children: [
                ['div', {class: ['name'], text: item.name}],
                ['div', {class: ['desc'], text: item.description}],
                ['div', {class: ['count'], text: item.count}],
                ['div', {class: ['price'], text: item.price + " руб."}],
                ['div', {class: ['sum'], text: item.sum + " руб."}],
            ]
        }]
    ));
}